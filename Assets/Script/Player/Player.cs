using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] protected float speed = 10;

    //[SerializeField] protected float padding = 0.8f;
    [SerializeField] protected float minX;
    [SerializeField] protected float maxX;
    [SerializeField] protected float minY;
    [SerializeField] protected float maxY;

    protected void Update()
    {
        float deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float deltaY = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        float posX = Mathf.Clamp(transform.position.x + deltaX, minX, maxX);
        float posY = Mathf.Clamp(transform.position.y + deltaY, minY, maxY);

        transform.position = new Vector2 (posX, posY);
    }
}
