using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    [SerializeField] protected GameObject playerBullet;
    [SerializeField] protected float bulletSpawnTime = 0.1f;

    protected void Start()
    {
        StartCoroutine(Shoot());
    }

    protected void Fire()
    {
        Instantiate(playerBullet, transform.position, Quaternion.identity);
    }

    IEnumerator Shoot()
    {
        while (true)
        {
            yield return new WaitForSeconds(bulletSpawnTime);
            Fire();
        }
    }
}
