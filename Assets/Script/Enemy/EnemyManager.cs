﻿using System.Collections;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private Transform spawnPoint;

    [Header("Enemy Formation")]
    [SerializeField] private EnemyFormationSO formationSquare;
    [SerializeField] private EnemyFormationSO formationRhombus;
    [SerializeField] private EnemyFormationSO formationTriangle;
    [SerializeField] private EnemyFormationSO formationRectangular;

    private void Start()
    {
        StartCoroutine(SpawnAndMoveEnemies());
    }

    private IEnumerator SpawnAndMoveEnemies()
    {
        yield return SpawnFormation(formationSquare);

        yield return new WaitForSeconds(5f);
        yield return MoveEnemiesToFormation(formationRhombus);

        yield return new WaitForSeconds(5f);
        yield return MoveEnemiesToFormation(formationTriangle);

        yield return new WaitForSeconds(5f);
        yield return MoveEnemiesToFormation(formationRectangular);
    }

    private IEnumerator SpawnFormation(EnemyFormationSO formation)
    {
        Vector2[] formationPositions = formation.formationPositions;

        foreach (Vector2 formationPos in formationPositions)
        {
            GameObject enemy = Instantiate(enemyPrefab, spawnPoint.position, Quaternion.identity);

            EnemyMove enemyMovement = enemy.GetComponent<EnemyMove>();
            enemyMovement.MoveToPos(new Vector3(spawnPoint.position.x + formationPos.x, spawnPoint.position.y + formationPos.y, spawnPoint.position.z));

            yield return new WaitForSeconds(0.1f);
        }
    }

    private IEnumerator MoveEnemiesToFormation(EnemyFormationSO formation)
    {
        Vector2[] formationPositions = formation.formationPositions;
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        for (int i = 0; i < formationPositions.Length && i < enemies.Length; i++)
        {
            Vector2 formationPos = formationPositions[i];
            GameObject enemy = enemies[i];

            EnemyMove enemyMovement = enemy.GetComponent<EnemyMove>();
            enemyMovement.MoveToPos(new Vector3(spawnPoint.position.x + formationPos.x, spawnPoint.position.y + formationPos.y, spawnPoint.position.z));

            yield return new WaitForSeconds(0.1f);
        }
    }
}
