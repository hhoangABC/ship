using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy Formation", menuName = "Enemy Formation")]
public class EnemyFormationSO : ScriptableObject
{
    public Vector2[] formationPositions;
}
