﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 3f;
    private Vector2 targetPosition;

    private int health = 5;
    private bool immortal = true;
    private float immortalTime = 23;

    private void Start()
    {
        StartCoroutine(DisableInvulnerability());
    }

    public void MoveToPos(Vector2 position)
    {
        targetPosition = position;
    }

    private void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (!immortal && collision.CompareTag("Bullet"))
        {
            health--;

            if (health <= 0)
            {
                PointManager.instance.AddPoints(1);
                Destroy(gameObject);
            }

            Destroy(collision.gameObject);
        }
    }

    private IEnumerator DisableInvulnerability()
    {
        yield return new WaitForSeconds(immortalTime);
        immortal = false;
    }
}
