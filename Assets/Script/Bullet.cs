﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] protected float bulletSpeed = 10;
    [SerializeField] protected float despawnDistance = 30;

    private float traveledDistance = 0;

    protected void Update()
    {
        transform.Translate(Vector2.up * bulletSpeed * Time.deltaTime);


        //Huỷ viên đạn
        traveledDistance += bulletSpeed * Time.deltaTime;
        if (traveledDistance >= despawnDistance)
        {
            Destroy(gameObject);
        }
    }
}