using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] protected Renderer meshRender;
    [SerializeField] protected float speed;

    protected void Update()
    {
        //Vector2 offset = meshRender.material.mainTextureOffset;
        //offset = offset + new Vector2(0, speed * Time.deltaTime);
        //meshRender.material.mainTextureOffset = offset;

        meshRender.material.mainTextureOffset += new Vector2(0, speed * Time.deltaTime);
    }
}
