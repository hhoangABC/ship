﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointManager : MonoBehaviour
{
    public static PointManager instance;
    [SerializeField] protected Text pointText;

    private int points = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        UpdatePointText();
    }

    public void AddPoints(int amount)
    {
        points += amount;
        UpdatePointText();
    }

    private void UpdatePointText()
    {
        pointText.text = "Point: " + points.ToString();
    }
}
